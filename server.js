const app = require('./app');
const http = require('http');
const { logger } = require('./configs');

const server = http.createServer(app);
const port = 3000;
server.listen(port);

logger.info('Server listening on port ' + port);