/**
 * @author Rajendra
 */

const mongoose = require('mongoose');
const appRoot = require('app-root-path');
const logger = require(`${appRoot}/configs/logger.config`);

const dbConfig = {
    "name": "sample",
    "host": "localhost",
    "port": 27017,
    "debug": true,
    "username": "",
    "password": "",
    "srvRecord": false
};
const mongoOpts = { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true };

var uri = '';
if (dbConfig.srvRecord) {
    uri = `mongodb+srv://${dbConfig.username}:${dbConfig.password}@${dbConfig.host}/${dbConfig.name}`;
} else {
    uri = `mongodb://${dbConfig.host}:${dbConfig.port}/${dbConfig.name}`;
    mongoOpts.user = dbConfig.username;
    mongoOpts.pass = dbConfig.password;
}

mongoose.connect(uri, mongoOpts).then(() => logger.info(`Database succesfully connected to ${uri}`))
    .catch((err) => {
        logger.error('Could not connect to the database. Exiting now...', err);
        process.exit();
    });

//Handle after initial connection was established
mongoose.connection.on('error', err => {
    logger.error(err);
});

//Handle disconnection issues
mongoose.connection.on('disconnected', () => {
    logger.error('Database disconnected.');
});

//set other properties here
mongoose.set('debug', dbConfig.debug);

module.exports = mongoose;