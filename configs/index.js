/**
 * @author Rajendra
 */

const datasource = require('./database.config');
const swagger = require('./swagger.config');
const logger = require('./logger.config');

//Export configured services
module.exports = {
    //Data source with connection
    datasource,
    //Swagger config object to handle API documentation
    swagger,
    // Logger for logging using winston
    logger
};