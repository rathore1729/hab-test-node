/**
 * @author Rajendra
 */

var winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const appRoot = require('app-root-path');

// define the custom settings for each transport (file, console)
var options = {
    file:{
        filename: "sample-%DATE%.log",
        dirname: `${appRoot}/logs`,
        prepend: true,
        maxsize: 5242880, //5MB
        maxFiles: 10,
        handleExceptions: true,
        json: false,
        colorize: false,
        datePattern: "YYYY-MM-DD",
        zippedArchive: true,
        level: "debug"
    },
    console:{
        json: true,
        handleExceptions: false,
        colorize: true,
        level: "debug"
    }
};

// instantiate a new Winston Logger with the settings defined above
var logger =  winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.printf(info => `${info.timestamp} ${info.level} :: ${info.message}`)
    ),
    transports: [
        new winston.transports.Console(options.console),
        new DailyRotateFile(options.file)
    ],
    exitOnError: false, // do not exit on handled exceptions
});

// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
    write: function (message, encoding) {
        // use the 'info' log level so the output will be picked up by both transports (file and console)
        logger.info(message);
    },
};

module.exports = logger;