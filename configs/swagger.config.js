/**
 * Swagger Configuration for API's documentation
 * @link https://www.npmjs.com/package/swagger-ui-express
 * @author Rajendra
 */
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

module.exports = {
    serve: swaggerUi.serve,
    setup: swaggerUi.setup(swaggerDocument, {explorer: true})
};