/**
 * @author Rajendra
 */

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const APP_SECRET = "sfjdf982sfk29932hdsjfh2992";
const createError = require('common-errors');

/**
 * Compare the user password in DB and input user password and revert the result
 * @param {*} inputPassword 
 * @param {*} userPassword 
 * @param {*} callback 
 */
module.exports.comparePasswords = (inputPassword, userPassword, callback) => {
    bcrypt.compare(inputPassword, userPassword, (err, isAuthenticated) => {
        if (err || !isAuthenticated) {
            callback(false);
        } else {
            callback(true);
        }
    });
}

/**
 * Get access token using user ID payload
 * @param {*} userId 
 * @param {*} callback 
 */
module.exports.getAccessToken = (userId, callback) => {
    const token = jwt.sign({ id: userId }, APP_SECRET, { expiresIn: '1d' });
    callback(token);
}

/**
 * Method to verify access token and authenticate request
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
module.exports.verifyToken = (req, res, next) => {
    let authHeader = req.headers.authorization;
    if (authHeader && `${authHeader}`.split(' ').length > 1) {
        let token = `${authHeader}`.split(' ')[1];
        jwt.verify(token, APP_SECRET, function (err, decoded) {
            if (err) {
                res.json({ status: "error", message: err.message, data: null });
            } else {
                // add user id to request
                req.body.userId = decoded.id;
                next();
            }
        });
    } else {
        let error = new createError.AuthenticationRequiredError('Authorization header missing!');
        res.status(401).send(error);
    }
}