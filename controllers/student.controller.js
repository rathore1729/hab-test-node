/**
 * @author Rajendra
 */
const studentService = require('../services/student.service');
const { logger } = require('../configs');

/**
 * Get list of all students
 * @param {Request} req 
 * @param {Response} res 
 */
exports.getAllStudents = (req, res, next) => {
    logger.debug(`Request received to get all students.`);
    studentService.getAllStudents((err, data) => {
        if (err) {
            res.status(err.status || 500).send(err);
        } else {
            res.send(data);
        }
    })
};

/**
 * @param {Request} req 
 * @param {Response} res 
 */
exports.addNewStudent = (req, res, next) => {
    let student = {};
    student.roll_no = req.body.roll_no;
    student.name = req.body.name;
    student.branch = req.body.branch;
    logger.debug(`Request received to add a new student`);
    studentService.addNewStudent(student, function (err, data) {
        if (err) {
            res.status(err.status || 500).send(err);
        } else {
            res.send(data);
        }
    });
};

/**
 * @param {Request} req 
 * @param {Response} res 
 */
exports.updateStudent = (req, res, next) => {
    let student = {};
    student.roll_no = req.params.id;
    student.name = req.body.name;
    student.branch = req.body.branch;
    logger.debug(`Request received to update student with roll_no: ${student.roll_no}`);
    studentService.updateStudent(student, function (err, data) {
        if (err) {
            res.status(err.status || 500).send(err);
        } else {
            res.send(data);
        }
    });
};

/**
 * @param {Request} req 
 * @param {Response} res 
 */
exports.removeStudent = (req, res, next) => {
    let roll_no = req.params.id;
    logger.debug(`Request received to delete student with roll_no: ${roll_no}`);
    studentService.removeStudent(roll_no, function (err, data) {
        if (err) {
            res.status(err.status || 500).send(err);
        } else {
            res.send(data);
        }
    });
};