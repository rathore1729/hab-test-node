/**
 * @author Rajendra
 */
const userService = require('../services/user.service');
const { logger } = require('../configs');

/**
 * Register a new user
 * @param {Request} req 
 * @param {Response} res 
 */
exports.signUpUser = (req, res, next) => {
    let user = {};
    user.email = req.body.email;
    user.password = req.body.password;
    user.name = req.body.name;

    logger.debug(`Request received to register new user with email: ${user.email}`);
    userService.signUpUser(user, (err, user) => {
        if (err) {
            res.status(err.status || 500).send(err);
        } else {
            res.send(user);
        }
    })
};

/**
 * @param {Request} req 
 * @param {Response} res 
 */
exports.loginUser = (req, res, next) => {
    let user = {};
    user.email = req.body.email;
    user.password = req.body.password;
    
    logger.debug(`Request received to login user with email: ${user.email}`);
    userService.loginUser(user, function (err, data) {
        if (err) {
            res.status(err.status || 500).send(err);
        } else {
            res.send(data);
        }
    });
};