/**
 * @author Rajendra
 */
const createError = require('common-errors');
const Student = require("../models/student.model");
const { logger } = require('../configs');

/**
 * Method to return list of students
 * @param {callback} callback 
 */
exports.getAllStudents = (callback) => {
    Student.find({}).then(students => {
        if (students && students.length > 0) {
            callback(null, students);
        } else {
            callback(null, []);
        }
    }, err => {
        callback(err, null)
    })
};

/**
 * Method to add a new student
 * @param {*} student 
 * @param {*} callback 
 */
exports.addNewStudent = (student, callback) => {
    let newStudent = new Student(student);
    newStudent.save().then(student => {
        if (student) {
            return callback(null, student);
        } else {
            return callback(new createError.NotSupportedError('Something went wrong!'));
        }
    }).catch(err => {
        logger.error(err);
    });
}

/**
 * Method to update details of the student
 * @param {*} student 
 * @param {*} callback 
 */
exports.updateStudent = (student, callback) => {
    Student.replaceOne({roll_no: student.roll_no}, student).then(data => {
        if (data && data.nModified > 0) {
            return callback(null, {success: true});
        } else {
            return callback(new createError.NotSupportedError(`Student not found with roll no ${student.roll_no} or Something went wrong!`));
        }
    }).catch(err => {
        logger.error(err);
    });
}

/**
 * Method to remove a student
 * @param {*} roll_no 
 * @param {*} callback 
 */
exports.removeStudent = (roll_no, callback) => {
    Student.deleteOne({roll_no: roll_no}).then(data => {
        if (data && data.deletedCount == 1) {
            return callback(null, { success: true});
        } else {
            return callback(new createError.NotSupportedError(`Student not found with roll no ${roll_no} or Something went wrong!`));
        }
    }).catch(err => {
        logger.error(err);
    });
}