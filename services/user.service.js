/**
 * @author Rajendra
 */
const createError = require('common-errors');
const User = require("../models/user.model");
const { logger } = require('../configs');
const authenticateUtil = require('../utils/authenticate.util');

/**
 * Register a new user
 * @param {User} user 
 * @param {callback} callback 
 */
exports.signUpUser = (user, callback) => {
    if (user.email && user.password && user.name) {
        let newUser = new User(user);
        newUser.save().then(user => {
            callback(null, { result: "user created successfully." })
        }, err => {
            callback(err, null)
        })
    } else {
        callback(new createError.ArgumentNullError('Please provide all the fields!'))
    }
};

/**
 * Login the user with user credentials
 * @param {User} user 
 * @param {callback} callback 
 */
exports.loginUser = (inputUser, callback) => {
    if (inputUser.email && inputUser.password) {
        User.findOne({ email: inputUser.email }).then(user => {
            if (user) {
                logger.debug(user);
                authenticateUtil.comparePasswords(inputUser.password, user.password, isAuthenticated => {
                    logger.debug(`Password matched: ${isAuthenticated}`);
                    if (isAuthenticated) {
                        authenticateUtil.getAccessToken(user._id, accessToken => {
                            logger.debug(`Received the access token : ${accessToken}`);
                            let userDetails = {
                                email: user.email,
                                access_token: accessToken
                            };
                            callback(null, userDetails)
                        })
                    } else {
                        callback(new createError.AuthenticationRequiredError('Authentication failed!'))
                    }
                })
            } else {
                callback(new createError.NotFoundError('Could not find the user with provided email!'), null)
            }
        }, err => {
            logger.error(err);
            callback(err, null)
        })
    } else {
        callback(new createError.ArgumentNullError('Please provide all the fields!'))
    }
};