var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
const SALT_ROUNDS = 12;

var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String
    },
    name: {
        type: String
    }
});

//Encrypt password before save/update user
UserSchema.pre('save', function (next) {
    var user = this;
    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) {
        return next();
    }

    bcrypt.hash(user.password, SALT_ROUNDS, function (err, hash) {
        if (err) {
            return next(err);
        }
        // override the cleartext password with the hashed one
        user.password = hash;
        next();
    });
});

module.exports = mongoose.model('userss', UserSchema);