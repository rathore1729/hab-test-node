var mongoose = require('mongoose');

var StudentSchema = new mongoose.Schema({
    roll_no: {
        type: Number,
        unique: true,
        required: true
    },
    name: {
        type: String
    },
    branch: {
        type: String
    }
});

module.exports = mongoose.model('students', StudentSchema);