/**
 * @author Rajendra
 */
const express = require("express");
const router = express.Router();
const userRoute = require('./user.routes');
const studentRoute = require('./student.routes');

router.use('/users', userRoute);
router.use('/students', studentRoute);

module.exports = router;