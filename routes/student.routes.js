/**
 * @author Rajendra Rathore
 */

const express = require('express');
const router = express.Router();
const studentController = require('../controllers/student.controller');
const authenticateUtil = require('../utils/authenticate.util');

// Route to get list of all students
router.get('/', authenticateUtil.verifyToken, studentController.getAllStudents);

// Route to add new student
router.post('/add', authenticateUtil.verifyToken, studentController.addNewStudent);

// Route to update student details
router.post('/update/:id', authenticateUtil.verifyToken, studentController.updateStudent);

// Route to remove a student
router.delete('/remove/:id', authenticateUtil.verifyToken, studentController.removeStudent);

module.exports = router;