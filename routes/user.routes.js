/**
 * @author Rajendra Rathore
 */

const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');

// Route to login a user and get access token
router.post('/login', userController.loginUser);

// Route to register new user
router.post('/signup', userController.signUpUser);

module.exports = router;